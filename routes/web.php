<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontEnd\HomeController@index');
Route::get('/about', 'FrontEnd\AboutController@index');
Route::get('/contact', 'FrontEnd\ContactController@index');
Route::get('/login', function() {
    return view('pages.loginPage');
});

Route::post('/contact/confirm', 'FrontEnd\ContactController@confirm');
Route::post('/contact/success', 'FrontEnd\ContactController@success');



Route::get('/admin', function () {
    return view('pages.admins.dashboard');
});

Route::get('/admin/contact', 'Admin\AdminContactController@list');
Route::get('/admin/contact/{id}', 'Admin\AdminContactController@detail');
Auth::routes();
Route::get('/admin/dashboard', 'Admin\DashboardController@index')->name('admin');
