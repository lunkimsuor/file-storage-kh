@extends('layouts.admin.adminLayout')

@section('title', 'Contact List')

@section('pageTitle', 'Contact List')

@section('content')
    <div class="d-flex justify-content-end mb-3">
        <button id="btnBulkEmails" class="btn btn-outline-secondary text-red text-uppercase"><i class="fa fa-envelope-o"></i>Emails</button>
    </div>
    <form class="row mb-2">
        <div class="col-6 d-flex">
            <select class="form-control">
                <option>Bulk Actions</option>
            </select>
            <button class="btn btn-outline-secondary text-uppercase ml-2">Apply</button>
            <select class="form-control ml-2">
                <option>pending</option>
                <option>complete</option>
            </select>
            <button class="btn btn-outline-secondary text-uppercase ml-2">Filter</button>
        </div>
        <div class="col-6 d-flex">
            <div class="ml-auto">
                <input type="text" class="form-control" placeholder="Search here...">  
            </div>
            <button class="btn btn-outline-secondary text-uppercase ml-2">Search</button>   
        </div>
    </form>

    @include('components.list.contact_list');

    @push('script')
        <script>
            $(function () {
                $(document).on('click', '.contact-row', function (event) {
                    let id = $(this).data('id');
                    window.location = `/admin/contact/${id}`;
                })
            })
        </script>
    @endpush
    
    <div class="d-flex justify-content-between">
        <p><b>Total items:</b> {{ count($contacts) }}</p>
        <ul class="pagination">
            <li class="page-item">
                <a class="page-link" href="#" aria-label="Previous">
                    <span aria-hidden="true" class="text-dark">< Previous</span>
                </a>
            </li>
            <li><a class="page-link text-dark" href="">1</a></li>
            <li><a class="page-link text-dark" href="">2</a></li>
            <li><a class="page-link text-dark" href="">3</a></li>
            <li><a class="page-link text-dark" href="">4</a></li>
            <li><a class="page-link text-dark" href="">5</a></li>
            <li>
                <a class="page-link" href="#" aria-label="Next">
                    <span aria-hidden="true" class="text-dark">Next ></span>
                </a>
            </li>
        </ul>
    </div>
@stop

