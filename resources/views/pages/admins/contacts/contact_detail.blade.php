@extends('layouts.admin.adminLayout')

@section('title', 'Contact Detail')

@section('pageTitle', 'Contact View')

@section('content')
    <div class="d-flex justify-content-end mb-2">
        <button class="ml-1 btn btn-sm btn-outline-secondary">Pending</button>
        <button class="ml-1 btn btn-sm btn-outline-secondary">In Progress</button>
        <button class="ml-1 btn btn-sm btn-outline-secondary">Completed</button>
    </div>

    @include('components.detail.contact_detail');

    <div class="d-flex justify-content-between">
        <a href="{{ url('admin/contact') }}" class="btn btn-outline-secondary">< Back</a>
        <button type="button" id="btnReply" class="btn btn-outline-secondary">Reply</button>
    </div>

    @include('components.modal.contact_reply');    

    @push('script')
        <script>
            $(function () {
                $('#btnReply').on('click', function (event) {
                    $('.reply-modal').modal('show');
                })
            })
        </script>
    @endpush

@stop

