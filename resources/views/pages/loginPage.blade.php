@extends('layouts.home.homeLayout')

@section('title')
    Customer Login
@stop

@section('content')
    <div class="row pt-3">
        
        @include('components.auth.login')

        <div class="col-lg-4 d-flex justify-content-center align-self-center">
            <h1>OR</h1>
        </div>
        
        @include('components.auth.register')

    </div>
@stop
