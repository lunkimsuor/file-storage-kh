@extends('layouts.home.homeLayout')

@section('title')
    {{ $page->page_title }}
@stop

@section('content')
    {!! $page->page_content !!}
@stop
