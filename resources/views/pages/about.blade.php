@extends('layouts.home.homeLayout')

@section('title')
    {{ $page->page_title }}
@stop

@section('pageTitle')
    {{ $page->page_title }} Us
@stop

@section('content')
     {!! $page->page_content !!} 
@stop
