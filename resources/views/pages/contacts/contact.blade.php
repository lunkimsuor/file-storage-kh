@extends('layouts.home.homeLayout')

@section('title', 'Contact')

@section('pageTitle', 'Contact')

@section('content')
    <form class="pb-5" method="post" action="/contact/confirm">
        <div class="form-group">
            <label for="input_name" >Name <span class="text-danger">*</span></label>
            <input type="text" class="form-control" name="name" id="inputName" required>
            @csrf
        </div>
        <div class="row">
            <div class="col form-group">
                <label for="input_email">Email <span class="text-danger">*</span></label>
                <input type="email" class="form-control" id="inputEmail" name="email" required>
            </div>
            <div class="col form-group">
                <label for="input_phoneNumber" >Phone Number</label>
                <input type="tel" class="form-control" id="inputPhoneNumber" name="phone">
            </div>
        </div>
        <div class="form-group">
            <label for="input_subject" >Subject <span class="text-danger">*</span></label>
            <input type="text" class="form-control" id="inputSubject" name="subject" required>
        </div>
        <div class="form-group">
            <label for="input_message">Message <span class="text-danger">*</span></label>
            <textarea style="min-height: 9rem" class="form-control" id="inputMessage" name="message" required></textarea>
        </div>
        <div class="d-flex justify-content-end">
        <!--<a href="{{ url('/contact/confirm') }}" class="btn btn-outline-secondary">Next ></a>-->
        <button type="submit" class="btn btn-info">Next ></button>
    </form>
@stop
