@extends('layouts.home.homeLayout')

@section('title', 'Contact')

@section('pageTitle', 'Contact Confirmation')

@section('content')
    <form class="pb-5" method="POST" action="/contact/success">
        @csrf
        <table class="table table-bordered">
            <tbody>
            <tr>
                <th style="min-width: 10rem">Name</th>
                <td class="col-8">{{ $contact->name }}</td>
            </tr>
            <tr>
                <th>Email</th>
                <td>{{ $contact->email }}</td>
            </tr>
            <tr>
                <th>Phone Number</th>
                <td>{{ $contact->phone }}</td>
            </tr>
            <tr>
                <th>Subject</th>
                <td>{{ $contact->subject }}</td>
            </tr>
            <tr>
                <th>Message</th>
                <td class="pb-5">{{ $contact->message }}</td>
            </tr>
            </tbody>
        </table>
        
        <div class="d-flex justify-content-between">
            <a href="{{ url('/contact') }}" class="btn btn-outline-secondary">< Back</a>
            <button type="submit" class="btn btn-outline-secondary">Submit</button>
        </div>
    </form>
@stop
