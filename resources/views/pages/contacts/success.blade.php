@extends('layouts.home.homeLayout')

@section('title', 'Contact')

@section('pageTitle', 'Contact Submitted')

@section('content')
    <div class="d-flex flex-column align-items-center">
        <div class="text-center my-5">
            <h3>Your message have been submitted to our system!</h3>
            <p>Our team will feedback to you soon, Thank you</p>
        </div>
        <a href="{{ url('/') }}" class="text-uppercase btn btn-outline-secondary">< Go Back Home Page</a>
    </div>
@stop
