<div class="modal fade reply-modal" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-uppercase" id="exampleModalLabel">Email</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="mb-4">
                    <div class="form-group">
                        <label for="inputTo">To</label>
                        <input type="text" class="form-control" id="inputTo">
                    </div>
                    <div class="form-group">
                        <label for="inputSubject">Subject</label>
                        <input type="text" class="form-control" id="inputSubject">
                    </div>
                    <div class="form-group">
                        <label for="inputMessage">Message</label>
                        <textarea style="min-height: 10rem" class="form-control" id="inputMessage"></textarea>
                    </div>
                </form>
                <div class="d-flex justify-content-between">
                    <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Send</button>
                </div>
            </div>
        </div>
    </div>
</div>