<table class="table table-bordered">
    <tbody>
    <tr>
        <th style="min-width: 10rem">Name</th>
        <td class="col-8">{{ $contact->name }}</td>
    </tr>
    <tr>
        <th>Email</th>
        <td>{{ $contact->email }}</td>
    </tr>
    <tr>
        <th>Phone Number</th>
        <td>{{ $contact->name }}</td>
    </tr>
    <tr>
        <th>Subject</th>
        <td>{{ $contact->subject }}</td>
    </tr>
    <tr>
        <th>Message</th>
        <td class="pb-4">{{ $contact->message }}</td>
    </tr>
    </tbody>
</table>
