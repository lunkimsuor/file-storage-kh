<div class="col-lg-4 card">
    <div class="card-header"><h3>Registration Form</h3></div>
    <div class="card-body">
        <form>
            <div class="form-group">
                <label>Your Email: </label>
                <input class="form-control" id="input_rEmail" type="text" placeholder="Enter your Email..." />
            </div>
            <div class="form-group">
                <label>Your Password: </label>
                <input class="form-control" id="input_rPassword" type="text" placeholder="Enter your Password..." />
            </div>
            <div class="form-group">
                <label>Confirm Password: </label>
                <input class="form-control" id="input_rComfirmPassword" type="text" placeholder="Enter your Password..." />
            </div>
            <div>
                <input type="checkbox" class="mr-2" />
                <label>Accept term & privacy</label>
            </div>
            <div class="d-flex justify-content-end">
                <button id="exampleModal" class="btn btn-info pl-4 pr-4" type="button" data-toggle="modal" data-target="#myModal">Register</button>
            </div>
        </form>

        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="d-flex">
                            <h4 class="modal-title">Registration Form</h4>
                            <button type="button" class="close ml-auto" data-dismiss="modal">&times;</button>
                        </div>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group">
                                <label>First Name: </label>
                                <input class="form-control" type="text" id="input_firstname" placeholder="Enter yoru first name"/>
                            </div>
                            <div class="form-group">
                                <label>Last Name: </label>
                                <input class="form-control" type="text" id="input_lastname" placeholder="Enter yoru last name"/>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-info" data-dismiss="modal">Register</button>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>