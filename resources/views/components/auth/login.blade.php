<div class="col-lg-4 card">
    <div class="card-header"><h3>Login Form</h3></div>
    <div class="card-body">
        <form>
            <div class="form-group">
                <label>Email: </label>
                <input class="form-control" id="input_email" type="text" placeholder="Enter your Email..." />
            </div>
            <div class="form-group">
                <label>Password: </label>
                <input class="form-control" id="input_password" type="text" placeholder="Enter your Password..." />
            </div>
            <div class="d-flex justify-content-end">
                <a href="">Forget Password?</a>
            </div>
            <div>
                <input type="checkbox" class="mr-2" />
                <label>Remember me</label>
            </div>
            <div class="d-flex justify-content-end mt-5">
                <button class="btn btn-info pl-4 pr-4" type="submit">Login</button>
            </div>
        </form>
    </div>
</div>