@extends('layouts.defaultLayout')

@section('body')
    <div class="container">
        <h1 class="mt-3 mb-3">@yield('pageTitle')</h1>
        <hr class="pb-3" />

        @include('layouts.admin.navLayout')

        @yield('content')
    </div>        
@stop
