@extends('layouts.defaultLayout')

@section('body')
    @include('layouts.home.navLayout')
    <div class="container">
        <h1 class="mt-5 pt-3 mb-3">@yield('pageTitle')</h1>
        <hr class="pb-3" />
    
        @yield('content')
    </div>
@stop

@section('footer')
    <div class="footer bg-dark text-center text-white fixed-bottom">@ 2020 - File Storage KH. ALL RIGHT RESERVED</div>
@stop