<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    protected $fillable = ['first_name', 'last_name', 'email', 'password', 'role_id'];
    protected $hidden = [
        'password', 'remember_token',
    ];
}
