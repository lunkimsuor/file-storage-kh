<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminContactController extends Controller
{

    

    public function list() {        
        $contacts = array((object) [
            'name' => 'Mr. kimsuor',
            'subject' => 'support',
            'email' => 'lunkimsuor@dev.com',
            'status' => 'pending',
            'date' => '03/14/2020',
            'message' => 'Nani'
        ], (object) [
            'name' => 'Mr. Davis',
            'subject' => 'support',
            'email' => 'davis@dev.com',
            'status' => 'pending',
            'date' => '03/17/2020',
            'message' => 'Nani'
        ], (object) [
            'name' => 'Ms. Dina',
            'subject' => 'support',
            'email' => 'dna@dev.com',
            'status' => 'pending',
            'date' => '03/20/2020',
            'message' => 'Nani'
        ]);

        return view('pages.admins.contacts.contact_list')->with('contacts', $contacts);
    }

    public function detail($id) {

        $contacts = array((object) [
            'name' => 'Mr. kimsuor',
            'subject' => 'support',
            'email' => 'lunkimsuor@dev.com',
            'status' => 'pending',
            'date' => '03/14/2020',
            'message' => 'Nani'
        ], (object) [
            'name' => 'Mr. Davis',
            'subject' => 'support',
            'email' => 'davis@dev.com',
            'status' => 'pending',
            'date' => '03/17/2020',
            'message' => 'Nani'
        ], (object) [
            'name' => 'Ms. Dina',
            'subject' => 'support',
            'email' => 'dna@dev.com',
            'status' => 'pending',
            'date' => '03/20/2020',
            'message' => 'Nani'
        ]);

        if ($id < count($contacts) && $id > -1) {
            return view('pages.admins.contacts.contact_detail')->with('contact', $contacts[$id-1]);
        } else {
            abort(404);
        }

        
    }

}

