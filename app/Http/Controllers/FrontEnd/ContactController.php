<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ContactController extends Controller
{

    public function index() {
        return view('pages.contacts.contact');
    }

    public function confirm(Request $request) {
        $contact_confirm = (object) [
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'phone' => $request->input('phone'),
            'subject' => $request->input('subject'),
            'message' => $request->input('message')
        ];
        return view('pages.contacts.confirm')->with('contact', $contact_confirm);
    }

    public function success(Request $req) {
        //print_r($req -> json());
        return view('pages.contacts.success');
    }
}
