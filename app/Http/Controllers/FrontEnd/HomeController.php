<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class HomeController extends Controller
{

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::ADMIN;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function index() {
        
        $page = (object) [
            'page_title' => 'Home',
            'page_content' => '
                <div class="text-center mt-5 pt-5">
                    <h1 class="font-italic">
                        File Storage KH
                    </h1>
                    <hr style="border-top: 1px solid" />
                    <h4 class="pt-4 mb-4 ">
                        Get Start with Us for Free
                    </h4>
                    <a class="btn btn-info pl-4 pt-2 pb-2 pr-4" href="/login">Login</a>
                </div>'
        ];
        
        return view('pages.homePage')->with('page', $page);
    }
}
