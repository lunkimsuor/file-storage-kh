<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function index() {
        $page = (object) [
            'page_title' => 'About',
            'page_content' => '
                <div class="">
                    <p class="pb-5">File storage founded in 2020</p>
                    <h6>Address: File Storage</h6>
                    <p>St. 160, Tuek Lauk 2, Tuol Kok, Phnom Penh City, Cambodia<p>
                </div>'
        ];

        return view('pages.about')->with('page', $page);
    }

}
